#!/bin/sh

RED='\033[0;31m'
GREEN='\033[0;32m'
RESET='\033[0m'

# parse parameters
while getopts hc: flag
do
  case "${flag}" in
    h)
        echo "Provide the URL to an EDC connector's data management API with the -c flag."
        exit 0
        ;;
    c)
        url=${OPTARG};;
    *) echo "Invalid option provided: ${OPTARG}";;
  esac
done

# ensure that url is set
if [ -z ${url+x} ]; then
  echo "${RED}No connector URL provided. Please use the -c flag to provide it.${RESET}"
  exit 1
fi

# Create asset
asset=$(cat <<EOF
{
    "asset": {
        "properties": {
            "asset:prop:id": "test-asset",
            "asset:prop:description": "description",
            "asset:prop:version": "1.0",
            "asset:prop:contenttype": "text/plain"
        }
    },
    "dataAddress": {
        "properties": {
            "type": "HttpData",
            "baseUrl": "https://samples.openweathermap.org/data/2.5/weather?q=London&appid=b1b15e88fa797225412429c1c50c122a1",
            "name": "London weather"
        }
    }
}
EOF
)

asset_status_code=$(curl --write-out "%{http_code}" --silent --output /dev/null \
  -X POST -H "Content-Type: application/json" -d "${asset}" "${url}/assets")

case $asset_status_code in
  2*) echo "${GREEN}Created asset.${RESET}" ;;
  409) echo "${RED}Failed to create asset: asset with this ID already exists.${RESET}" ;;
  4*) echo "${RED}Failed to create asset: request was malformed.${RESET}" ;;
  *) echo "${RED}Failed to create asset. Received response code: $asset_status_code${RESET}" ;;
esac


# Create policy
policy=$(cat <<EOF
{
    "id": "test-policy",
    "policy": {
        "permissions": [
            {
                "action": {
                    "type": "USE"
                },
                "edctype": "dataspaceconnector:permission"
            }
        ]
    }
}
EOF
)

policy_status_code=$(curl --write-out "%{http_code}" --silent --output /dev/null \
  -X POST -H "Content-Type: application/json" -d "${policy}" "${url}/policydefinitions")

case $policy_status_code in
  2*) echo "${GREEN}Created policy.${RESET}" ;;
  4*) echo "${RED}Failed to create policy: request was malformed.${RESET}" ;;
  *) echo "${RED}Failed to create policy. Received response code: $policy_status_code${RESET}" ;;
esac


# Create contract definition
contractdef=$(cat <<EOF
{
    "accessPolicyId": "test-policy",
    "contractPolicyId": "test-policy",
    "selectorExpression": {
        "criteria": [
            {
                "left": "asset:prop:id",
                "op": "=",
                "right": "test-asset"
            }
        ]
    },
    "id": "test-contractdef"
}
EOF
)

contractdef_status_code=$(curl --write-out "%{http_code}" --silent --output /dev/null \
  -X POST -H "Content-Type: application/json" -d "${contractdef}" "${url}/contractdefinitions")

case $contractdef_status_code in
  2*) echo "${GREEN}Created contract definition.${RESET}" ;;
  4*) echo "${RED}Failed to create contract definition: request was malformed.${RESET}" ;;
  *) echo "${RED}Failed to create contract definition. Received response code: $contractdef_status_code${RESET}" ;;
esac


exit 0
