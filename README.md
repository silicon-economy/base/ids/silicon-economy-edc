> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.


<div align="center">
  <h2 align="center">Silicon Economy EDC</h2>
  A reference implementation of the <a href="https://github.com/eclipse-edc/Connector">Connector from the Eclipse Dataspace Components (EDC)
</a> for Silicon Economy projects.
</div>


## Table of Contents
- [About](#about)
  - [Background](#background)
- [Getting Started](#getting-started)
  - [Prerequirements](#prerequirements)
  - [Quick Start](#quick-start)
- [Contributing](#contributing)
- [License](#license)
- [Developers](#developers)


## About
The *Silicon Economy EDC* is a configured version from the
[Connector of the Eclipse Dataspace Components (EDC)](https://github.com/eclipse-edc/Connector).
It is used and specialized to easily integrate Silicon Economy components with the IDS.

When running this project, it will start two full EDC instances with PostreSQL databases for persistence.

### Background
The [International Data Spaces (IDS)](https://internationaldataspaces.org/) initiative, formed
by different research and industry partners across the globe, aims to create a decentralized
ecosystem to enable secure data exchange under the consideration of data sovereignty and
interoperability. One core component that is needed to participate is an IDS Connector, for
example the [Eclipe Dataspace Connector (EDC)](https://github.com/eclipse-dataspaceconnector/DataSpaceConnector)
that follows the [IDS Reference Architecture Model (RAM)](https://www.internationaldataspaces.org/wp-content/uploads/2019/03/IDS-Reference-Architecture-Model-3.0.pdf).


## Getting Started

### Prerequirements
The following things are needed to run this EDC setup:

- Docker
- Docker-compose
- Postman

### Quick Start
Running the following steps will start two connectors, a provider and a consumer, both of which consist of a control
plane, a data plane, and a PostgreSQL database for persistence.

#### Initial Setup
First, we will start the system and seed some example data into the provider EDC that can later be requested by a consumer:

1. Start the system (provider and consumer): `docker-compose up -d --build`
2. Add example offer to provider: `./scripts/seed-data.sh -c http://localhost:8182/api/v1/data`
3. Your setup is ready and your provider already provides data!

#### Performing a Transfer
All requests required to perform a data transfer are prepared in a `Postman` collection along with a `Postman`
environment. The requests contain scripts that store information required for the next request in the environment, so
no changes need to be made to the requests.

1. Import Postman collection via File > Import...: `postman/SELE-EDC.postman.json`
2. Import Postman environment via File > Import...: `postman/SELE-EDC.postman_environment.json`
3. Open environment, fill the variable `consumer_backend_url` (both fields) with the URL you want to send data to and
save it
   > If you do not have a backend available, you can use an external service like `https://webhook.site`. This will
   > create a URL you can send data to and let you view all incoming requests. Visit the website in your browser and
   > use the generated link for `consumer_backend_url`.
4. In the upper right corner, ensure that `SELE EDC` is the selected environment
5. Open Collections on the left side
6. Run request `Initiate negotiation`
   > This will contact the consumer EDC control plane and tries to negotiate a contract for a data transfer. In case of
   > a success, it returns a *negotation ID*.
7. Run request `Get negotiation`
   > This uses the previous *negotation ID* to request the contract negotiation and stores the *agreement ID*. In order
   > for the next request to work, the response to this request needs to contain the following: `"state": "CONFIRMED"`.
   > If you see `"state": "REQUESTED"`, wait a few seconds and run this request again.
8. Run request `Initiate transfer`
   > This will start the actual data transfer by telling the consumer to request the data asset from the provider based
   > on the previous contract agreement. This will send the data to the consumer backend.
9. Wait for a moment and see transferred data (weather data in JSON format) at your provided backend URL from step 3

#### Useful Links
Here you will find some useful links from the data management API:

**Provider EDC Control Plane:**
- Data Assets: http://localhost:8182/api/v1/data/assets
- Policy Definitions: http://localhost:8182/api/v1/data/policydefinitions
- Contract Definitions: http://localhost:8182/api/v1/data/contractdefinitions
- Contract Negotiations: http://localhost:8182/api/v1/data/contractnegotiations
- Contract Agreements: http://localhost:8182/api/v1/data/contractagreements
- Transfer Process: http://localhost:8182/api/v1/data/transferprocess

**Consumer EDC Control Plane:**
- Data Assets: http://localhost:9192/api/v1/data/assets
- Policy Definitions: http://localhost:9192/api/v1/data/policydefinitions
- Contract Definitions: http://localhost:9192/api/v1/data/contractdefinitions
- Contract Negotiations: http://localhost:9192/api/v1/data/contractnegotiations
- Contract Agreements: http://localhost:9192/api/v1/data/contractagreements
- Transfer Process: http://localhost:9192/api/v1/data/transferprocess


## Contributing
Contributions to this project are greatly appreciated! Code changes are handled over pull requests.
Detailed information can be found in the `CONTRIBUTING.md` guidelines and the `CODE_OF_CONDUCT.md`.


## License
Copyright © 2022 Open Logistics Foundation. The project is licensed under the [Open Logistics
License 1.0](https://openlogisticsfoundation.org/licenses/). For details on the licensing terms,
see the `LICENSE` file.


## Developers
- Ronja Quensel ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
- Haydar Qarawlus ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
- Malte Hellmeier ([Fraunhofer ISST](https://www.isst.fraunhofer.de/en.html))
