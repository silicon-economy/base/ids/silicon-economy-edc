/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

repositories {
  mavenCentral()
  maven {
    url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
  }
  maven {
    url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
  }
}

plugins {
  `java-library`
  id("application")
  id("com.github.johnrengelman.shadow") version "7.1.2"
}

val jupiterVersion: String by project

dependencies {
  // Minimal control plane, incl. state machine, incl. default stores
  implementation("org.eclipse.edc:control-plane-core:0.0.1-milestone-7")

  // PostgreSQL
  implementation("org.postgresql:postgresql:42.3.7")
  implementation("org.eclipse.edc:control-plane-sql:0.0.1-milestone-7")
  implementation("org.eclipse.edc:transaction-local:0.0.1-milestone-7")
  implementation("org.eclipse.edc:sql-pool-apache-commons:0.0.1-milestone-7")

  // International Data Spaces (IDS) protocol (infomodel, multipart, serialization, ...)
  implementation("org.eclipse.edc:ids:0.0.1-milestone-7")

  // Configuration over properties file and certificate storage in a local vault
  implementation("org.eclipse.edc:configuration-filesystem:0.0.1-milestone-7")
  implementation("org.eclipse.edc:vault-filesystem:0.0.1-milestone-7")

  // Provides a no-operation identity provider in the dataspace (similar to DAPS)
  implementation("org.eclipse.edc:iam-mock:0.0.1-milestone-7")

  // REST API to use the control plane
  implementation("org.eclipse.edc:data-management-api:0.0.1-milestone-7")

  // Data plane integration
  implementation("org.eclipse.edc:data-plane-transfer-sync:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-transfer-client:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-selector-core:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-selector-client:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-selector-spi:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-selector-api:0.0.1-milestone-7")

  // JWT (required for signing data plane tokens)
  implementation("org.eclipse.edc:jwt-core:0.0.1-milestone-7")

  // Testing things
  //testImplementation("org.junit.jupiter:junit-jupiter-api:${jupiterVersion}")
  //testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${jupiterVersion}")
}

application {
  mainClass.set("org.eclipse.edc.boot.system.runtime.BaseRuntime")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
  exclude("**/pom.properties", "**/pom.xm")
  mergeServiceFiles()
  archiveFileName.set("sele-edc-controlplane.jar")
}
