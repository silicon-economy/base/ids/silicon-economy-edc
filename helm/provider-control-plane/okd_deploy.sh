#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=sele-edc}"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i provider-control-plane .
# Ensure image stream picks up the new docker image right away
oc import-image provider-control-plane:1.0.0
