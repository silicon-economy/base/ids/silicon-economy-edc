#!/bin/bash

RED=$(printf '\e[31m')
GREEN=$(printf '\e[32m')
YELLOW=$(printf '\e[33m')
BLUE=$(printf '\e[34m')
RESET=$(printf '\e[0m')

if [ -n "$1" ]; then
  config_dir_path=$1
  echo "${BLUE}Using provided path to configuration: ${config_dir_path}${RESET}"
else
  config_dir_path=/configuration
  echo "${YELLOW}No path to configuration supplied via argument, using default: ${config_dir_path}${RESET}"
fi

# wait until all things are up and running
sleep 8

# read provider config (control and data plane)
# create a map
typeset -A provider_controlplane_env
while read line;
  do
    # ignore empty lines
    [ -z "$line" ] && continue
    # ignore comment lines
    [[ "$line" == \#* ]] && continue

    # split line at "=" and add map entry
    lineParts=(${line//=/ })
    provider_controlplane_env[${lineParts[0]}]=${lineParts[1]}
  done < "$config_dir_path"/provider/controlplane.env

typeset -A provider_dataplane_env
while read line;
  do
    [ -z "$line" ] && continue
    [[ "$line" == \#* ]] && continue

    lineParts=(${line//=/ })
    provider_dataplane_env[${lineParts[0]}]=${lineParts[1]}
  done < "$config_dir_path"/provider/dataplane.env

# register provider data plane
provider_input=$(cat <<EOF
{
    "edctype": "dataspaceconnector:dataplaneinstance",
    "id": "http-data-plane",
    "url": "http://provider-dataplane:${provider_dataplane_env[WEB_HTTP_CONTROL_PORT]}${provider_dataplane_env[WEB_HTTP_CONTROL_PATH]}/transfer",
    "allowedSourceTypes": [
        "HttpData", "HttpProvision"
    ],
    "allowedDestTypes": [
        "HttpData", "HttpProxy"
    ],
    "properties": {
        "publicApiUrl": "http://provider-dataplane:${provider_dataplane_env[WEB_HTTP_PUBLIC_PORT]}${provider_dataplane_env[WEB_HTTP_PUBLIC_PATH]}"
    }
}
EOF
)
provider_status_code=$(curl --write-out "%{http_code}" --silent --output /dev/null \
 -X POST -H "Content-Type: application/json" -d "${provider_input}" \
 "http://provider-controlplane:${provider_controlplane_env[WEB_HTTP_DATAPLANE_PORT]}${provider_controlplane_env[WEB_HTTP_DATAPLANE_PATH]}/instances")
if [ "$provider_status_code" -eq 204 ] ; then
  echo "${GREEN}Registered provider data plane.${RESET}"
else
  echo "${RED}Failed to register provider data plane. Received response code: $provider_status_code${RESET}"
fi

unset provider_controlplane_env
unset provider_dataplane_env
unset provider_input

# read consumer config (control and data plane)
typeset -A consumer_controlplane_env
while read line;
  do
    [ -z "$line" ] && continue
    [[ "$line" == \#* ]] && continue

    lineParts=(${line//=/ })
    consumer_controlplane_env[${lineParts[0]}]=${lineParts[1]}
  done < "$config_dir_path"/consumer/controlplane.env

typeset -A consumer_dataplane_env
while read line;
  do
    [ -z "$line" ] && continue
    [[ "$line" == \#* ]] && continue

    lineParts=(${line//=/ })
    consumer_dataplane_env[${lineParts[0]}]=${lineParts[1]}
  done < "$config_dir_path"/consumer/dataplane.env

# register consumer data plane
consumer_input=$(cat <<EOF
{
    "edctype": "dataspaceconnector:dataplaneinstance",
    "id": "http-data-plane",
    "url": "http://provider-dataplane:${consumer_dataplane_env[WEB_HTTP_CONTROL_PORT]}${consumer_dataplane_env[WEB_HTTP_CONTROL_PATH]}/transfer",
    "allowedSourceTypes": [
        "HttpData", "HttpProvision"
    ],
    "allowedDestTypes": [
        "HttpData", "HttpProxy"
    ],
    "properties": {
        "publicApiUrl": "http://provider-dataplane:${consumer_dataplane_env[WEB_HTTP_PUBLIC_PORT]}/${consumer_dataplane_env[WEB_HTTP_PUBLIC_PATH]}"
    }
}
EOF
)
consumer_status_code=$(curl --write-out "%{http_code}" --silent --output /dev/null \
 -X POST -H "Content-Type: application/json" -d "${consumer_input}" \
 "http://consumer-controlplane:${consumer_controlplane_env[WEB_HTTP_DATAPLANE_PORT]}${consumer_controlplane_env[WEB_HTTP_DATAPLANE_PATH]}/instances")
if [ "$consumer_status_code" -eq 204 ] ; then
  echo "${GREEN}Registered consumer data plane.${RESET}"
else
  echo "${RED}Failed to register consumer data plane. Received response code: $consumer_status_code${RESET}"
fi

exit 0
