/*
 * Copyright 2022 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */

repositories {
  mavenCentral()
  maven {
    url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
  }
}

plugins {
  `java-library`
  id("application")
  id("com.github.johnrengelman.shadow") version "7.1.2"
}

val jupiterVersion: String by project

dependencies {
  implementation("org.eclipse.edc:data-plane-core:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-api:0.0.1-milestone-7")
  implementation("org.eclipse.edc:data-plane-http:0.0.1-milestone-7")

  implementation("org.eclipse.edc:http:0.0.1-milestone-7")

  implementation("org.eclipse.edc:configuration-filesystem:0.0.1-milestone-7")
}

application {
  mainClass.set("org.eclipse.edc.boot.system.runtime.BaseRuntime")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
  exclude("**/pom.properties", "**/pom.xm")
  mergeServiceFiles()
  archiveFileName.set("sele-edc-dataplane.jar")
}
