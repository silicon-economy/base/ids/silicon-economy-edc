# Changelog
All notable changes to this project will be documented in this file

## [0.1.0] - 2022-12-16

First initial version.

### Added
- A configurable EDC control plane
- A configurable EDC data plane
- PostgreSQL database
- Postman collection and initialization scripts
- Helm charts
- Docker-compose setup
- Documentation on how to deploy and perform a data transfer between a consumer and  a provider
- GitLab CI pipeline for conventional commit checks
